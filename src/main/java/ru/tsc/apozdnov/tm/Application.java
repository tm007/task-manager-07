package ru.tsc.apozdnov.tm;

import ru.tsc.apozdnov.tm.constant.ArgumentConstant;
import ru.tsc.apozdnov.tm.constant.TerminalConstant;
import ru.tsc.apozdnov.tm.model.Command;
import ru.tsc.apozdnov.tm.util.ConvertByteUtil;

import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        if (processArgumentTask(args)) System.exit(0);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String cmd = scanner.nextLine();
            processCommandTask(cmd);
        }
    }

    public static void processCommandTask(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConstant.ABOUT:
                showAbout();
                break;
            case TerminalConstant.VERSION:
                showVersion();
                break;
            case TerminalConstant.HELP:
                showHelp();
                break;
            case TerminalConstant.INFO:
                showSystemInfo();
                break;
            case TerminalConstant.EXIT:
                close();
                break;
            default:
                showFaultCommnand(command);
                break;
        }
    }

    public static void processArgumentTask(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.INFO:
                showSystemInfo();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            default:
                showFaultArgument(arg);
                break;
        }
    }

    public static boolean processArgumentTask(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgumentTask(arg);
        return true;
    }

    public static void close() {
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.println("**** Welcome to Task Manager ****");
    }

    public static void showFaultArgument(String fault) {
        System.err.printf("Fault... This argument `%s` not supported...  \n", fault);
    }

    public static void showFaultCommnand(String fault) {
        System.err.printf("Fault... This command `%s` not supported...  \n", fault);
    }

    public static void showAbout() {
        System.out.println("[About]");
        System.out.println("[Name: Aleksandr Pozdnov]");
        System.out.println("[E-mail: apozdnov@t1.com]");
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableprocessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableprocessors);
        final long freememory = runtime.freeMemory();
        final String freeMemoryFormat = ConvertByteUtil.formatBytes(freememory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = Long.toString(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormatValue = ConvertByteUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (isMemoryLimit ? "no limit" : maxMemoryFormatValue);
        System.out.println("Maximum memory : " + maxMemoryFormat);
        final long totatMemory = runtime.totalMemory();
        final String totalMemoryFormat = ConvertByteUtil.formatBytes(totatMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totatMemory - freememory;
        final String usedMemoryFormat = ConvertByteUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryFormat);
    }

    public static void showVersion() {
        System.out.println("1.7.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

}
